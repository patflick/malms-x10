
# For local adaptions, edit config.mak
# See config.mak.example for skeleton
-include config.mak

BUILD_TARGETS = Hello malms/Test

EXECUTABLES = $(addprefix $(BUILDDIR)/,$(BUILD_TARGETS))
FLAGS = -sourcepath $(SRCDIR)
#FLAGS += -assert -debugpositions -DEBUG=true # -sourcepath $(SRCDIR)
# for an optimized build use these flags instead:
FLAGS += -O -NO_CHECKS -cxx-prearg -O3
EXE = .x10exe
SRCDIR = $(abspath src)
DOCDIR = $(abspath docs)
BUILDDIR = $(abspath build)
BIN_DIR = $(abspath build/bin)
PATH := $(X10_PATH):$(PATH)
DEPENDS = $(shell find ${SRCDIR} -name "*.x10")

X10RT_FLAGS = # default

all: $(addsuffix $(EXE),$(EXECUTABLES))

$(BUILDDIR)/%$(EXE): ${SRCDIR}/%.x10 ${DEPENDS}
	mkdir -p $(BUILDDIR);
	cd $(BUILDDIR) &&\
	x10c++ $(FLAGS) $(X10RT_FLAGS) $< -o $(notdir $@)

$(DOCDIR)/index.html: $(DEPENDS)
	mkdir -p $(DOCDIR)
	cd $(DOCDIR) &&\
		x10doc -sourcepath $(SRCDIR) $(SRCDIR)/Hello.x10 

.PHONY: clean run docs
clean:
	rm -rf $(BUILDDIR)
	rm -rf $(DOCDIR)
	
run: $(addsuffix $(EXE),$(EXECUTABLES))
	$(addsuffix $(EXE);,$(EXECUTABLES))
	
docs: $(DOCDIR)/index.html
