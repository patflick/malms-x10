package malms;
import x10.lang.Boolean;
import x10.util.IndexedMemoryChunk;
/**
 * @author		Patrick Flick	<patrick.flick@gmail.com>
 * @version		0.1
 * @since		2012-05-24
 * 
 * Implements the loser tree for merging.
 */
public class LoserTree[T] {

	private var m_ik: int;
	private var m_k: int;
	private var m_offset: int;
	private var m_cmp: (T,T)=>int;
	private var m_losers: IndexedMemoryChunk[Loser[T]];
	
	/**
	 * Constructs a LoserTree.
	 * 
	 * @param k			The amount of sequences.
	 * @param someKey	Any instance of type [T], the value does not matter.
	 * @param cmp		A comparator for the elements of type [T].
	 */
	public def this(k:int, someKey: T, cmp:(T,T)=>Int) {
		// init variables
		this.m_ik = k;
		this.m_k = nextPowOf2(k);
		this.m_offset = this.m_k;
		this.m_cmp = cmp;
		
		
		// init loser tree
		this.m_losers = IndexedMemoryChunk.allocateUninitialized[Loser[T]](2*m_k);
		for (var i: int = 0; i < this.m_ik + this.m_k; i++) {
			this.m_losers(i) = new Loser[T](Boolean.FALSE, -1, someKey);
		}
		// mark all additional sequences as supremum
		for (var i: int = this.m_ik + this.m_k; i < 2*this.m_k; i++) {
			this.m_losers(i) = new Loser[T](Boolean.TRUE, -1, someKey);
		}
	}
	

	/**
	 * Inserts the given key into the empty LoserTree.
	 * 
	 * @param key		The key (value) of the element inserted.
	 * @param source	The source sequence of the key given.
	 */
	public def insert_start(key: T, source: int) {
		insert_start(key, source, Boolean.FALSE);	
	}

	/**
	 * Inserts the given key into the empty LoserTree.
	 * 
	 * @param key		The key (value) of the element inserted.
	 * @param source	The source sequence of the key given.
	 * @param supremum	Sets the element as a artificial supremum
	 * 					if set to TRUE.
	 */
	public def insert_start(key: T, source: int, supremum: Boolean) {
		var pos: int = this.m_k + source;
		this.m_losers(pos).supremum = supremum;
		this.m_losers(pos).source = source;
		this.m_losers(pos).key = key;		
	}
	
	/**
	 * Returns the source sequence of the currently minmal element.
	 * 
	 * @return The source sequence of the currently minimal element in
	 * 			the tree.
	 */
	public def get_min_source(): int {
		return this.m_losers(0).source;
	}
	
	/**
	 * Initializes the loser tree. This is a recursive procedure.
	 * 
	 * @param root	The root from where the initialization starts.
	 * @return		The index of the winner.
	 */
	private def init_winner(root: int): int {
		if (root >= this.m_k) {
			return root;
		} else {
			val left: int = init_winner(2 * root);
			val right: int = init_winner(2 * root + 1);
			if (this.m_losers(right).supremum || (!this.m_losers(left).supremum && this.m_cmp(this.m_losers(left).key,this.m_losers(right).key) <= 0)) {
				// left child is smaller or equal, write right child and promote left child
				this.m_losers(root) = this.m_losers(right);
				return left;
			} else {
				// right child is smaller, write left child and promote right child
				this.m_losers(root) = this.m_losers(left);
				return right;
			}
		}
	}
	
	/**
	 * Initializes the loser tree.
	 * 
	 * Has to be called _after_ the insert_start, and before any
	 * delete_min_insert are called.
	 */
	public def init() {
		this.m_losers(0) = this.m_losers(init_winner(1));
	}
	
	/**
	 * Deletes the current min and inserts the new given key for the
	 * same sequence.
	 * 
	 * @param key		The key (value) to be inserted.
	 * @param supremum	Sets the element as a artificial supremum
	 * 					if set to TRUE.
	 */
	public def delete_min_insert(key: T, supremum: Boolean) {
		assert(this.m_losers(0).source != -1);
		
		val source = this.m_losers(0).source;
		var curWinner : Loser[T] = new Loser[T](supremum, source, key);
		for(var pos: int = (this.m_k + source)/2; pos > 0; pos /= 2) {
			if (curWinner.supremum ||
					(!this.m_losers(pos).supremum &&
					  this.m_cmp(this.m_losers(pos).key, curWinner.key) <= 0)) {
				// swap
				var tmp : Loser[T] = this.m_losers(pos);
				this.m_losers(pos) = curWinner;
				curWinner = tmp;
			}
		}
		this.m_losers(0) = curWinner;
	}
	
	
	/**
	 * Calculates the next power of 2 of the given int x.
	 * 
	 * @param x		The input value.
	 * @return		The next power of 2 of the input value.
	 */
	private static def nextPowOf2(val x: Int): Int {
		var v : Int = x;
		v--;
		v |= v >> 1;
		v |= v >> 2;
		v |= v >> 4;
		v |= v >> 8;
		v |= v >> 16;
		v |= v >> 32;
		v++;
		return v;
	}
}
