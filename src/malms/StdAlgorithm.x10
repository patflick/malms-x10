package malms;
import x10.util.IndexedMemoryChunk;
import x10.util.List;
import x10.util.Pair;
import x10.util.Random;
import x10.lang.Math;

/**
 * @author		Patrick Flick	<patrick.flick@gmail.com>
 * @version		0.1
 * @since		2012-06-12
 * 
 * Implements standard algorithms used by the malleable sorting routines.
 */
public class StdAlgorithm {
	
	/**
	 * Finds the lower bound of a value in a sorted sequence by the use
	 * of binary search.
	 * This is a modified and ported version of the GNU STL lower_bound code.
	 * 
	 * @param data			The input data.
	 * @param first			The index marking the beginning of the sequence
	 * 						to be searched.
	 * @param last			The (exclusive) index marking the end of the
	 * 						sequence to be searched.
	 */
	public static def lower_bound[T](var data: IndexedMemoryChunk[T], var first: long, var last: long, var value: T, cmp:(T,T)=>Int): long {
		var len: long = last - first;
		while (len > 0)
		{
			var half: long = len >> 1;
			var middle: long = first;
			middle += half;
			if (cmp(data(middle), value) < 0)
			{
				first = middle;
				++first;
				len = len - half - 1;
			}
			else
				len = half;
		}
		return first;
	}
	
	/**
	 * Switches the values in a at positions i and j.
	 * 
	 * @param a		The array of values.
	 * @param i		The index of the first value to be switched.
	 * @param j		The index of the second value to be switched.
	 */
	private static def exch[T](a:IndexedMemoryChunk[T], i: long, j: long):void {
		val temp = a(i);
		a(i) = a(j);
		a(j) = temp;
	}
	
	
	/**
	 * Partitions the sequence [begin,end) according to the pivot value.
	 * This is basically the partition routine from the GNU STL Sort,
	 * slightly modified.
	 * 
	 * @param inData		The input data.
	 * @param begin			The index marking the beginning of the sequence.
	 * @param end			The (exclusive) index marking the end of the sequence.
	 * @param pivot			The value used as pivot for the partition.
	 * @param cmp			A comparator for the input type [T].
	 */
	public static def partitionPivot[T](var inData: IndexedMemoryChunk[T], var begin: long, var end: long, var pivot: T, cmp:(T,T)=>Int): long {
		while (true) {
			while (cmp(inData(begin), pivot) < 0) ++begin;
			--end;
			while (cmp(pivot, inData(end)) < 0) --end;
			if (begin >= end) return begin;
			exch(inData, begin, end);
			++begin;
		}
	}
	

	
	/**
	 * Implements the quickselect selection algorithm. Partitions the elements in
	 * [begin,end) so that the k-th element in sorted order is at the k-th position
	 * in the sequence given by [begin,end). The k-th element can be accessed by
	 * (begin+k).
	 * 
	 * @param inData		The input data.
	 * @param begin			The index marking the beginning of the sequence.
	 * @param end			The (exclusive) index marking the end of the sequence.
	 * @param k				The element to be selected (k-th).
	 * @param cmp			A comparator for the input type [T].
	 */
	public static def quickselect[T](var inData: IndexedMemoryChunk[T], var begin: long, var end: long, var k: long, cmp:(T,T)=>Int) {
		var rand: Random = new Random();
		var pivot: long;
		var cut: long;
		while (end - begin > 1) {
			pivot = begin + (Math.abs(rand.nextLong()) % (end-begin));
			cut = partitionPivot(inData, begin, end, inData(pivot), cmp);
			if (cut-begin > k) {
				end = cut;
			} else {
				k -= (cut-begin);
				begin = cut;
			}
		}
	}
	
	
	/**
	 * Sorts the given array into ascending order.
	 * @param a the array to be sorted
	 * @param cmp the comparison function to use
	 */
	public static def sort[T](a:Array[T], cmp:(T,T)=>Int) {
		qsort[T](a.raw(), 0, (a.size-1), cmp);
	}

	/**
	 * Sorts the given array into ascending order.
	 * @param a the array to be sorted
	 */
	public static def sort[T](a:Array[T]){T<:Comparable[T]} {
		sort[T](a, (x:T,y:T) => x.compareTo(y));
	}

	/**
	 * Sorts the given IndexedMemoryChunk into ascending order using quicksort.
	 * 
	 * @param a				The input data.
	 * @param lo			The index marking the beginning of the sequence.
	 * @param hi			The (inclusive) index marking the end of the sequence.
	 * @param cmp			A comparator for the input type [T].
	 */
	public static def qsort[T](a:IndexedMemoryChunk[T], lo:long, hi:long, cmp:(T,T)=>Int) {
		if (hi <= lo) return;
		var l:long = lo - 1;
		var h:long = hi;
		while (true) {
			while (cmp(a(++l), a(hi))<0);
			while (cmp(a(hi), a(--h))<0 && h>lo);
			if (l >= h) break;
			exch(a, l, h);
		}
		exch[T](a, l, hi);
		qsort[T](a, lo, l-1, cmp);
		qsort[T](a, l+1, hi, cmp);
	}
}
