package malms;

/**
 * @author		Patrick Flick	<patrick.flick@gmail.com>
 * @version		0.1
 * @since		2012-05-24
 * 
 * An Interface for a general workpacket.
 */
public interface WorkPacket {
	public def work(): void;
}
