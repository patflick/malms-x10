package malms;
import x10.lang.Boolean;
import x10.util.IndexedMemoryChunk;


/**
 * @author		Patrick Flick	<patrick.flick@gmail.com>
 * @version		0.1
 * @since		2012-06-19
 * 
 * Implements a packet for sorting.
 */
public class SortPacket[T] implements WorkPacket {

	private var array: IndexedMemoryChunk[T];
	private var begin: long;
	private var end: long;
	private var cmp:(T,T)=>Int;
	
	/**
	 * Constructs a SortPacket instance with the parameters for sorting.
	 * 
	 * @param array			The input data.
	 * @param begin			The index marking the beginning of the sequence.
	 * @param end			The (inclusive) index marking the end of the sequence.
	 * @param cmp			A comparator for the input type [T].
	 */
	public def this(array:IndexedMemoryChunk[T], begin:long, end:long, cmp:(T,T)=>Int) {
		this.array = array;
		this.begin = begin;
		this.end = end;
		this.cmp = cmp;
	}
	
	/**
	 * Does the actual sorting with the via the contructor given parameters.
	 */
	public def work(): void {
		StdAlgorithm.qsort(array, begin, end, cmp);
	}
}
