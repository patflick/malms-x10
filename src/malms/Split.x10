package malms;
import x10.util.IndexedMemoryChunk;
import x10.util.List;
import x10.util.Pair;
import x10.util.Random;
import x10.lang.Math;

/**
 * @author		Patrick Flick	<patrick.flick@gmail.com>
 * @version		0.1
 * @since		2012-06-12
 * 
 * Implements the splitting operation.
 */
public class Split {
	/**
	 * Performs one step of the medianSplit algorithm.
	 * 
	 * @param inData		The input data.
	 * @param first			Indeces pointing at the beginning of the currently
	 * 						active range in each sequence.
	 * @param last			Indeces pointing at the end (exclusive) of the
	 * 						currently active range in each sequence.
	 * @param current		Indeces pointing at the middle of the currently
	 * 						active range in each sequence.
	 * @param k				The number of sequences.
	 * @param params		The wrapper around the parameters reducePrefixSize
	 * 						and N, new values are returned in this wrapper.
	 * @param cmp			A comparator for the input type [T].
	 */
	private static def medianSplit[T](var inData: IndexedMemoryChunk[T], var first: IndexedMemoryChunk[long], var last: IndexedMemoryChunk[long], var current: IndexedMemoryChunk[long], val k: int, var params: SplittingParameters, cmp:(T,T)=>Int) {
		// (1.1) get medians of all sequences
		for (var j: int = 0; j < k; ++j) {
			if (first(j) != last(j))
				current(j) = first(j) + (last(j)-first(j))/2;
		}
		
		// (1.2) find weighted partition of the values at the current splitters
		var partitionSeq: IndexedMemoryChunk[PartitionElement[T]] = IndexedMemoryChunk.allocateUninitialized[PartitionElement[T]](k);
		var c: int = 0;
		for (var j: int = 0; j < k; ++j) {
			if (first(j) != last(j)) {
				partitionSeq(c) = new PartitionElement[T](j, inData(current(j)));
				c++;
			}
		}

		// sort showed to be faster than a weighted partition (for packet numbers up to at least 800)
		StdAlgorithm.qsort(partitionSeq, 0l, (c-1) as long, (x: PartitionElement[T],y: PartitionElement[T]) => cmp(x.key, y.key));
		
		// get weighted median of medians of all sequences
		var weight: long = 0;
		var i: int = 0;
		while (weight < params.N/2 && i < c) {
			weight += last(partitionSeq(i).source) - first(partitionSeq(i).source);
			i++;
		}
		i--;
		
		// get median value
		var paket: int = partitionSeq(i).source;
		var medianValue: T = inData(current(paket));
		
		// calc new splitters
		for (var j: int = 0; j < k; ++j) {
			if (j != paket) {
				if (cmp(inData(current(j)), medianValue) < 0) {
					current(j) = StdAlgorithm.lower_bound(inData, current(j), last(j), medianValue, cmp);
				} else if (cmp(inData(current(j)), medianValue) > 0) {
					current(j) = StdAlgorithm.lower_bound(inData, first(j), current(j), medianValue, cmp);
				}
			}
		}
		
		// summing elements
		var sum_els: long = 0;
		for (var j: int = 0; j < k; ++j) {
			sum_els += current(j) - first(j);
		}

		if (sum_els < params.reducePrefixSize) {
			// go in upper half
			for (var j: int = 0; j < k; ++j) {
				first(j) = current(j);
			}
			params.reducePrefixSize -= sum_els;
			params.N -= sum_els;
		} else {
			// go in lower half
			for (var j: int = 0; j < k; ++j) {
				last(j) = current(j);
			}
			params.N = sum_els;
		}
		
	}
	
	/**
	 * Calculates splitters for the given sorted sequences, such that all the
	 * values lying below the splitters in the sequences are smaller than all
	 * the values lying above the splitters in the sequences.
	 * 
	 * @param inData		The input data array. The sorted sequences are
	 * 						lying in this IndexedMemoryChunk.
	 * @param sequences		An array of Indexpairs. Each IndexPair defines one
	 * 						sequence with [begin,end) in inData, where end is
	 * 						handled as an exlcusive index. The length of this
	 * 						array is the number of sorted sequences.
	 * @param outSplitters	The output array for the found splitters.
	 * @param prefixSize	The parameter that gives the number of elements
	 * 						that have to lie below the found splitters.
	 * @param cmp			A comparator for the input type [T].
	 */
	public static def split[T](inData: IndexedMemoryChunk[T], var sequences:Array[IndexPair], var outSplitters: IndexedMemoryChunk[long], prefixSize: long, cmp:(T,T)=>Int) {
		val k: int = sequences.size;
		var first: IndexedMemoryChunk[long] = IndexedMemoryChunk.allocateUninitialized[long](k);
		var last: IndexedMemoryChunk[long] = IndexedMemoryChunk.allocateUninitialized[long](k);
		var current: IndexedMemoryChunk[long] = IndexedMemoryChunk.allocateUninitialized[long](k);
		
		// init splitters
		for (var i: int = 0; i < k; ++i) {
			first(i) = sequences(i).begin;
			last(i) = sequences(i).end;
		}
		
		// init N
		var params: SplittingParameters = new SplittingParameters(prefixSize, 0);
		
		for (var i: int = 0; i < k; ++i) {
			params.N += last(i) - first(i);
		}
		
		var reduceUntil: long = k + 16;
		
		// median split for reducing
		while (params.N > reduceUntil) {
			// reduce
			medianSplit(inData, first, last, current, k, params, cmp);
		}
		
		// TODO quickselect the remaining elements
		var remaining: IndexedMemoryChunk[PartitionElement[T]] = IndexedMemoryChunk.allocateUninitialized[PartitionElement[T]](params.N);
		var partitionIndex: int = 0;
		for (var j: int = 0; j < k; ++j) {
			for (var i: long = first(j); i < last(j); ++i) {
				remaining(partitionIndex) = new PartitionElement[T](j, inData(i));
				partitionIndex++;
			}
		}
		
		StdAlgorithm.quickselect(remaining, 0l, params.N, params.reducePrefixSize-1 , (x: PartitionElement[T],y: PartitionElement[T]) => cmp(x.key, y.key));
		//ArrayUtils.qsort(remaining, 0, (params.N-1) as int, (x: PartitionElement[T],y: PartitionElement[T]) => cmp(x.key, y.key));
		
		/* adapt splitters result of partition */
		for (var i: int = 0; i < params.N; ++i) {
			var paket: int = remaining(i).source;
			if (i < params.reducePrefixSize) {
				first(paket)++;
			} else {
				last(paket)--;
			}
		}
		
		/* save results */
		for (var i: int = 0; i < k; ++i) {
			outSplitters(i) = last(i);
		}
		
	}
}
