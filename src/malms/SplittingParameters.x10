package malms;
/**
 * @author		Patrick Flick	<patrick.flick@gmail.com>
 * @version		0.1
 * @since		2012-06-13
 * This class defines SplittingParameters. It is a wrapper around
 * the parameters used for mediumSplit which are changed inside the function.
 * The changed parameters are passed in the next call.
 */
public class SplittingParameters {
	/**
	 * The reducePrefixSize parameter.
	 */
	public var reducePrefixSize: long;
	/**
	 * The N parameter (remaining size).
	 */
	public var N: long;
	
	/**
	 * Constructs SplittingParameters from the given values.
	 * 
	 * @param reducePrefixSize	The reducePrefixSize parameter.
	 * @param N					The N parameter.
	 */
	public def this(reducePrefixSize: long, N: long) {
		this.reducePrefixSize = reducePrefixSize;
		this.N = N;
	}
	
	/**
	 * Constructs a default SplittingParameters.
	 */
	public def this() {
		this.reducePrefixSize = 0;
		this.N = 0;
	}
}
