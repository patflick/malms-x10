package malms;
import x10.lang.Boolean;
import x10.util.IndexedMemoryChunk;
import x10.util.ArrayList;
import x10.util.List;
import x10.util.concurrent.Lock;


/**
 * @author		Patrick Flick	<patrick.flick@gmail.com>
 * @version		0.1
 * @since		2012-05-24
 * 
 * Implements a simple queue (for workpackets) with locked thread-safe functions.
 */
public class WorkQueue {

	private var list: List[WorkPacket];
	private var lock: Lock;
	
	/**
	 * Constructs a WorkQueue.
	 */
	public def this() {
		this.list = new ArrayList[WorkPacket]();
		this.lock = new Lock();
	}
	
	/**
	 * Pops an element of the queue and does the work of the popped element.
	 * Return whether the queue is empty.
	 * 
	 * @return 	TRUE if the queue is empty and no work has been done, FALSE otherwise.
	 */
	public def workOne(): Boolean {
		lock.lock();
		if (!list.isEmpty()) {
			var result: WorkPacket = list.getFirst();
			list.removeFirst();
			lock.unlock();
			
			// do the work
			result.work();
			
			return Boolean.TRUE;
		} else {
			lock.unlock();
			return Boolean.FALSE;
		}
	}
	
	/**
	 * Returns whether the queue is empty. This function is Thread-Safe.
	 * 
	 * @return True if the Queue is empty and False otherwise.
	 */
	public def isEmpty(): Boolean {
		lock.lock();
		var result: Boolean = list.isEmpty();
		lock.unlock();
		return result;
	}
	
	/**
	 * Pushes an element to the end of the Queue. This function is Thread-Safe.
	 */
	public def push_back(var element: WorkPacket): void {
		lock.lock();
		list.add(element);
		lock.unlock();
	}

}
