package malms;
/**
 * @author		Patrick Flick	<patrick.flick@gmail.com>
 * @version		0.1
 * @since		2012-05-26
 * This class defines a IndexPair, which is a pair of indeces (begin,end)
 * which define a mutable Range.
 */
public class IndexPair {
	/**
	 * The first index, marks the beginning of an index range.
	 */
	public var begin: long;
	/**
	 * The second index, marks the exlcusive end of an index range.
	 */
	public var end: long;
	
	/**
	 * Constructs IndexPair from the given values.
	 * 
	 * @param begin	The first (beginning) index.
	 * @param end	The second (end) exclusive index.
	 */
	public def this(begin: long, end: long) {
		this.begin = begin;
		this.end = end;
	}
	
	/**
	 * Constructs a default IndexPair.
	 */
	public def this() {
		this.begin = 0;
		this.end = 0;
	}
}
