
package malms;
import x10.util.IndexedMemoryChunk;
import x10.util.Random;
import x10.array.Array;
import x10.util.Timer;

/**
 * @author		Patrick Flick	<patrick.flick@gmail.com>
 * @version		0.1
 * @since		2012-05-27
 * 
 * Runs async parallel timings of malms using workpackets (includes sort, split, merge).
 */
public class TimePacketMalms {
	
	/**
	 * Runs tests and outputs the success to stdout.
	 */
	public static def time() {
		Console.OUT.println("Running WorkPacket timings with N=" + Place.MAX_PLACES  + " X10-Places");
		
		// big case
		timeAndPrint(100000, 4, 4);
		timeAndPrint(1000000, 4, 4);
		timeAndPrint(10000000, 4, 4);
		timeAndPrint(100000000, 4, 4);
	}
	
	/**
	 * Runs a parallel async test of malms using workpackets with the given
	 * input size, number of sequences and number of threads used. 
	 * The test parameters and results are printed to stdout.
	 * 
	 * @param inputSize		The input size for the test.
	 * @param numSeq		The number of sequences for the test.
	 * @param numThreads	The number of threads used for the packet version of malms
	 */
	private static def timeAndPrint(inputSize: int, numSeq: int, numThreads: int) {
		var timeMS: long = timePacketMalms(inputSize, numSeq, numThreads);
		Console.OUT.println(inputSize + ";" + numSeq + ";" + numThreads + ";" + timeMS);
	}
	
	/**
	 * Runs a parallel async test of malms using workpackets with the given
	 * input size, number of sequences and number of threads used. 
	 * The test parameters and results are printed to stdout.
	 * 
	 * @param inputSize		The input size for the test.
	 * @param numSeq		The number of sequences for the test.
	 * @param numThreads	The number of threads used for the packet version of malms
	 */
	private static def timePacketMalms(inputSize: int, numSeq: int, numThreads: int): long {
		val inData: IndexedMemoryChunk[int] = IndexedMemoryChunk.allocateUninitialized[int](inputSize);
		val outData: IndexedMemoryChunk[int] = IndexedMemoryChunk.allocateUninitialized[int](inputSize);
		val checkData: IndexedMemoryChunk[int] = IndexedMemoryChunk.allocateUninitialized[int](inputSize);
		
		val queue: WorkQueue = new WorkQueue();
		
		// init with random ints
		var rand: Random = new Random();
		for (var i: int = 0; i < inputSize; ++i) {
			inData(i) = rand.nextInt();
		}
		
		val beginTimeMS: long;
		beginTimeMS = Timer.milliTime();
		
		// get sequences
		val seqs: Array[IndexPair] = new Array[IndexPair](numSeq);
		val basicSize: int = inputSize/numSeq;
		var remainder: int = inputSize % numSeq;
		var curIndex: int = 0;
		for (var i: int = 0; i < numSeq; ++i) {
			var size: int = basicSize;
			if (remainder > 0) {
				size++;
				remainder--;
			}
			seqs(i) = new IndexPair(curIndex, curIndex+size);
			curIndex += size;
		}
		
		// sort parts
		for (i in 0..(numSeq-1)) {
			queue.push_back(new SortPacket[int](inData, seqs(i).begin, seqs(i).end-1, (x: Int,y: Int) => x.compareTo(y)));
		}
		
		finish for (i in 0..(numThreads-1)) async {
			while (queue.workOne()) {
				// work one packet
			}
		}

		// init splitter array
		var outIndex: long = 0;
		var splitters: IndexedMemoryChunk[IndexedMemoryChunk[long]] = IndexedMemoryChunk.allocateUninitialized[IndexedMemoryChunk[long]](numSeq+1);
		splitters(0) = IndexedMemoryChunk.allocateUninitialized[long](numSeq);
		for (var i: int = 0; i < numSeq; ++i) {
			splitters(0)(i) = seqs(i).begin;
		}
		
		var seqOffset: IndexedMemoryChunk[long] = IndexedMemoryChunk.allocateUninitialized[long](numSeq);
		seqOffset(0) = 0l;
		for (var i: int = 1; i < numSeq; ++i) {
			seqOffset(i) = seqOffset(i-1) + (seqs(i-1).end-seqs(i-1).begin);
		}
		
		// split
		for (i in 0..(numSeq-1)) {
			splitters(i+1) = IndexedMemoryChunk.allocateUninitialized[long](numSeq);
			queue.push_back(new SplitPacket[int](inData, seqs, splitters(i+1), seqOffset(i)+seqs(i).end-seqs(i).begin, (x: Int,y: Int) => x.compareTo(y)));
		}
		
		// work on the queue, until all work-packets are finished
		finish for (i in 0..(numThreads-1)) async {
			while (queue.workOne()) {
				// work one packet
			}
		}
		
		/* reorder results */
		val mergeSeqs: Array[Array[IndexPair]] = new Array[Array[IndexPair]](numSeq);
		for (var i: int = 0; i < numSeq; ++i) {
			mergeSeqs(i) = new Array[IndexPair](numSeq);
			for (var j: int = 0; j < numSeq; ++j) {
				mergeSeqs(i)(j) = new IndexPair(splitters(i)(j), splitters(i+1)(j));
			}
		}
		
		// merge
		for (i in 0..(numSeq-1)) {	
			queue.push_back(new MergePacket[int](inData , outData, mergeSeqs(i),  seqOffset(i),  (x: Int,y: Int) => x.compareTo(y)));
		}
		
		// work on the queue, until all work-packets are finished
		finish for (i in 0..(numThreads-1)) async {
			while (queue.workOne()) {
				// work one packet
			}
		}
		
		return Timer.milliTime() - beginTimeMS;
	}
}
