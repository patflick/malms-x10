package malms;
import x10.util.IndexedMemoryChunk;
import x10.util.List;
import x10.util.Pair;

/**
 * @author		Patrick Flick	<patrick.flick@gmail.com>
 * @version		0.1
 * @since		2012-05-26
 * 
 * Implements the multi sequence merge operation.
 */
public class Merge {
	
	/**
	 * Merge multiple sorted sequences into one sorted sequence.
	 * 
	 * @param inData		The input data. The sorted sequences are ordered
	 * 						linearly in this IndexedMemoryChunk.
	 * @param outData		The output data. The sorted sequence is written
	 * 						into this output array.
	 * @param sequences		An array of Indexpairs. Each IndexPair defines one
	 * 						sequence with [begin,end) in inData, where end is
	 * 						handled as an exlcusive index. The length of this
	 * 						array is the number of sorted sequences.
	 * @param outIndex		The output is written to the index range
	 * 						[outIndex,...) in outData.
	 * @param cmp			A comparator for the input type [T].
	 */
	static def multiSeqMerge[T](inData: IndexedMemoryChunk[T], var outData: IndexedMemoryChunk[T], var sequences:Array[IndexPair], var outIndex: long, cmp:(T,T)=>Int) {
		var k: int = sequences.size;
		var someKey: T = inData(0);
		
		// create loser tree
		val lt: LoserTree[T] = new LoserTree[T](k, someKey, cmp);
		
		// fill loser tree
		var sequencesLeft: int = 0;
		for (var i: int = 0; i < k; ++i) {
			if (sequences(i).end - sequences(i).begin > 0) {
				lt.insert_start(inData(sequences(i).begin), i, Boolean.FALSE);
				sequencesLeft++;
			} else {
				lt.insert_start(someKey, i, Boolean.TRUE);
			}
		}
		
		// init loser tree
		lt.init();
		
		// do the merge
		while (sequencesLeft > 0) {
			var min_source: int = lt.get_min_source();
			outData(outIndex) = inData(sequences(min_source).begin);
			outIndex++;
			sequences(min_source).begin++;
			if (sequences(min_source).end - sequences(min_source).begin > 0) {
				lt.delete_min_insert(inData(sequences(min_source).begin), Boolean.FALSE);
			} else {
				// sequence is now empty
				lt.delete_min_insert(someKey, Boolean.TRUE);
				sequencesLeft--;
			}
		}
		
	}
}
