package malms;
import x10.lang.Boolean;
import x10.util.IndexedMemoryChunk;
import x10.array.Array;


/**
 * @author		Patrick Flick	<patrick.flick@gmail.com>
 * @version		0.1
 * @since		2012-06-19
 * 
 * Implements a packet for splitting.
 */
public class SplitPacket[T] implements WorkPacket {

	private var array: IndexedMemoryChunk[T];
	private var sequences:Array[IndexPair];
	private var outSplitters: IndexedMemoryChunk[long];
	private var prefixSize: long;
	private var cmp:(T,T)=>Int;
	
	/**
	 * Constructs a SplitPacket instance with the parameters for splitting.
	 * 
	 * @param inData		The input data array. The sorted sequences are
	 * 						lying in this IndexedMemoryChunk.
	 * @param sequences		An array of Indexpairs. Each IndexPair defines one
	 * 						sequence with [begin,end) in inData, where end is
	 * 						handled as an exlcusive index. The length of this
	 * 						array is the number of sorted sequences.
	 * @param outSplitters	The output array for the found splitters.
	 * @param prefixSize	The parameter that gives the number of elements
	 * 						that have to lie below the found splitters.
	 * @param cmp			A comparator for the input type [T].
	 */
	public def this(array: IndexedMemoryChunk[T], var sequences:Array[IndexPair], var outSplitters: IndexedMemoryChunk[long], prefixSize: long, cmp:(T,T)=>Int) {
		this.array = array;
		this.sequences = sequences;
		this.outSplitters = outSplitters;
		this.prefixSize = prefixSize;
		this.cmp = cmp;
	}
	
	/**
	 * Does the actual splitting with the via the contructor given parameters.
	 */
	public def work(): void {
		Split.split[T](array, sequences, outSplitters, prefixSize, cmp);
	}
}
