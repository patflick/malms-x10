
package malms;
import x10.util.IndexedMemoryChunk;
import x10.util.Random;
import x10.array.Array;

/**
 * @author		Patrick Flick	<patrick.flick@gmail.com>
 * @version		0.1
 * @since		2012-05-27
 * 
 * Runs sequential tests for the splitting function.
 */
public class TestSplit {
	
	/**
	 * Runs tests and outputs the success to stdout.
	 */
	public static def test() {
		testAndPrint(1000, 4, 20);
		testAndPrint(987, 3, 543);
		testAndPrint(15637, 17, 4532);
		testAndPrint(1234, 1, 200);
		testAndPrint(1000000, 8, 250000);
		testAndPrint(1000000, 48, 500000);
		testAndPrint(1000000, 200, 750000);
		
		// some special cases
		testAndPrint(1, 200, 1);
		testAndPrint(1, 1, 0);
		testAndPrint(2, 50, 2);
	}
	
	/**
	 * Runs a test of split with the given input size and number of sequences,
	 * where the splitters are calcuated such that they split rank elements.
	 * The test parameters and results are printed to stdout.
	 * 
	 * @param inputSize	The input size for the test.
	 * @param numSeq	The number of sequences for the test.
	 * @param rank		The number of elements to be splitted.
	 */
	private static def testAndPrint(inputSize: int, numSeq: int, rank: int) {
		Console.OUT.print("Input Size: " + inputSize + "  \tSequences: " + numSeq + "\t\t");
		var success: Boolean = testSplit(inputSize, numSeq, rank);
		if (success) {
			Console.OUT.println("[SUCCESS]");
		} else {
			Console.OUT.println("[FAILURE]");
		}
	}
	
	/**
	 * Runs a test of split with the given input size and number of sequences,
	 * where the splitters are calcuated such that they split rank elements.
	 * Returns TRUE when the test succeeded and FALSE otherwise.
	 * 
	 * @param inputSize	The input size for the test.
	 * @param numSeq	The number of sequences for the test.
	 * @param rank		The number of elements to be splitted.
	 * @return			TRUE if the test succeeded, FALSE otherwise.
	 */
	private static def testSplit(inputSize: int, numSeq: int, rank: long): Boolean {
		val inData: IndexedMemoryChunk[int] = IndexedMemoryChunk.allocateUninitialized[int](inputSize);
		val outData: IndexedMemoryChunk[int] = IndexedMemoryChunk.allocateUninitialized[int](rank);
		
		// init with random ints
		var rand: Random = new Random();
		for (var i: int = 0; i < inputSize; ++i) {
			inData(i) = rand.nextInt();
		}
		
		// get sequences
		val seqs: Array[IndexPair] = new Array[IndexPair](numSeq);
		val basicSize: int = inputSize/numSeq;
		var remainder: int = inputSize % numSeq;
		var curIndex: int = 0;
		for (var i: int = 0; i < numSeq; ++i) {
			var size: int = basicSize;
			if (remainder > 0) {
				size++;
				remainder--;
			}
			seqs(i) = new IndexPair(curIndex, curIndex+size);
			curIndex += size;
		}
		
		// sort parts
		for (var i: int = 0; i < numSeq; ++i) {
			StdAlgorithm.qsort(inData, seqs(i).begin, seqs(i).end-1, (x: Int,y: Int) => x.compareTo(y));
		}

		// now split
		var splitters: IndexedMemoryChunk[long] = IndexedMemoryChunk.allocateUninitialized[long](numSeq);
		Split.split(inData, seqs, splitters, rank, (x: Int,y: Int) => x.compareTo(y));
		
		// output data
		var outIndex: int = 0;
		for (var i: int = 0; i < numSeq; ++i) {
			for (var j: long = seqs(i).begin; j < splitters(i); ++j) {
				outData(outIndex) = inData(j);
				outIndex++;
			}
		}
		
		// sort out data
		StdAlgorithm.qsort(outData, 0l, rank-1, (x: Int,y: Int) => x.compareTo(y));
		
		// sort in data for comparison
		StdAlgorithm.qsort(inData, 0l, (inputSize-1) as long, (x: Int,y: Int) => x.compareTo(y));
		
		// compare
		var correct: Boolean = Boolean.TRUE;
		for (var i: int = 0; i < rank; ++i) {
			if (inData(i) != outData(i)) {
				correct = Boolean.FALSE;
				Console.OUT.println("FAIL at index " + i + " inData(i)=" + inData(i) + ", outData(i)=" + outData(i));
			}
		}
		
		// output success or failure
		return correct;
	}
}
