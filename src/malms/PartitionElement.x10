package malms;
import x10.lang.Boolean;
/**
 * @author		Patrick Flick	<patrick.flick@gmail.com>
 * @version		0.1
 * @since		2012-06-12
 * 
 * Defines an PartitionElement.
 */
public class PartitionElement[T] {

	/**
	 * The source sequence from which the key was taken.
	 */
	public var source : int;
	/**
	 * The key (value) of the element.
	 */
	public var key : T;
	
	/**
	 * Constructs a PartitionElement.
	 * @param source	Sets the source.
	 * @param key		Sets the key.
	 */
	public def this(source:int, key:T) {
		this.source = source;
		this.key = key;
	}
}
