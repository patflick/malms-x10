
package malms;
import x10.util.IndexedMemoryChunk;
import x10.util.Random;
import x10.array.Array;

/**
 * @author		Patrick Flick	<patrick.flick@gmail.com>
 * @version		0.1
 * @since		2012-05-27
 * 
 * Runs sequential tests for the multiway merge function.
 */
public class TestMerge {
	
	/**
	 * Runs tests and outputs the success to stdout.
	 */
	public static def test() {
		testAndPrint(1000, 4);
		testAndPrint(987, 3);
		testAndPrint(15637, 17);
		testAndPrint(1234, 1);
		testAndPrint(1000000, 8);
		testAndPrint(1000000, 48);
		testAndPrint(1000000, 200);
		
		// some special cases
		testAndPrint(1, 200);
		testAndPrint(1, 1);
		testAndPrint(2, 50);
	}
	
	/**
	 * Runs a test of merge with the given input size and number of sequences.
	 * The test parameters and results are printed to stdout.
	 * 
	 * @param inputSize	The input size for the test.
	 * @param numSeq	The number of sequences for the test.
	 */
	private static def testAndPrint(inputSize: int, numSeq: int) {
		Console.OUT.print("Input Size: " + inputSize + "  \tSequences: " + numSeq + "\t\t");
		var success: Boolean = testMultiSeqMerge(inputSize, numSeq);
		if (success) {
			Console.OUT.println("[SUCCESS]");
		} else {
			Console.OUT.println("[FAILURE]");
		}
	}
	
	/**
	 * Runs a test of merge with the given input size and number of sequences.
	 * Returns TRUE when the test succeeded and FALSE otherwise.
	 * 
	 * @param inputSize	The input size for the test.
	 * @param numSeq	The number of sequences for the test.
	 * @return			TRUE if the test succeeded, FALSE otherwise.
	 */
	private static def testMultiSeqMerge(inputSize: int, numSeq: int): Boolean {
		val inData: IndexedMemoryChunk[int] = IndexedMemoryChunk.allocateUninitialized[int](inputSize);
		val outData: IndexedMemoryChunk[int] = IndexedMemoryChunk.allocateUninitialized[int](inputSize);
		
		// init with random ints
		var rand: Random = new Random();
		for (var i: int = 0; i < inputSize; ++i) {
			inData(i) = rand.nextInt();
		}
		
		// get sequences TODO calc sizes dyn
		val seqs: Array[IndexPair] = new Array[IndexPair](numSeq);
		val basicSize: int = inputSize/numSeq;
		var remainder: int = inputSize % numSeq;
		var curIndex: int = 0;
		for (var i: int = 0; i < numSeq; ++i) {
			var size: int = basicSize;
			if (remainder > 0) {
				size++;
				remainder--;
			}
			seqs(i) = new IndexPair(curIndex, curIndex+size);
			curIndex += size;
		}
		
		// sort parts
		for (var i: int = 0; i < numSeq; ++i) {
			StdAlgorithm.qsort(inData, seqs(i).begin, seqs(i).end-1, (x: Int,y: Int) => x.compareTo(y));
		}

		// now merge
		var outIndex: long = 0;
		Merge.multiSeqMerge[int](inData , outData, seqs, outIndex,  (x: Int,y: Int) => x.compareTo(y));
		
		// sort in data for comparison
		StdAlgorithm.qsort(inData, 0l, (inputSize-1) as long, (x: Int,y: Int) => x.compareTo(y));
		
		// compare
		var correct: Boolean = Boolean.TRUE;
		for (var i: int = 0; i < inputSize; ++i) {
			if (inData(i) != outData(i)) {
				correct = Boolean.FALSE;
				Console.OUT.println("FAIL at index " + i + " inData(i)=" + inData(i) + ", outData(i)=" + outData(i));
			}
		}
		
		// output success or failure
		return correct;
	}
}
