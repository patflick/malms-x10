package malms;
import x10.lang.Boolean;
import x10.util.IndexedMemoryChunk;
import x10.array.Array;


/**
 * @author		Patrick Flick	<patrick.flick@gmail.com>
 * @version		0.1
 * @since		2012-06-19
 * 
 * Implements a packet for merging.
 */
public class MergePacket[T] implements WorkPacket {

	private var inData: IndexedMemoryChunk[T];
	private var outData: IndexedMemoryChunk[T];
	private var sequences:Array[IndexPair];
	private var outIndex: long;
	private var cmp:(T,T)=>Int;
	
	/**
	 * Constructs a MergePacket instance with the parameters for merging.
	 * 
	 * @param inData		The input data. The sorted sequences are ordered
	 * 						linearly in this IndexedMemoryChunk.
	 * @param outData		The output data. The sorted sequence is written
	 * 						into this output array.
	 * @param sequences		An array of Indexpairs. Each IndexPair defines one
	 * 						sequence with [begin,end) in inData, where end is
	 * 						handled as an exlcusive index. The length of this
	 * 						array is the number of sorted sequences.
	 * @param outIndex		The output is written to the index range
	 * 						[outIndex,...) in outData.
	 * @param cmp			A comparator for the input type [T].
	 */
	public def this(inData: IndexedMemoryChunk[T], var outData: IndexedMemoryChunk[T], var sequences:Array[IndexPair], var outIndex: long, cmp:(T,T)=>Int) {
		this.inData = inData;
		this.outData = outData;
		this.sequences = sequences;
		this.outIndex = outIndex;
		this.cmp = cmp;
	}
	
	/**
	 * Does the actual splitting with the via the contructor given parameters.
	 */
	public def work(): void {
		Merge.multiSeqMerge[T](inData, outData, sequences, outIndex, cmp);
	}
}
