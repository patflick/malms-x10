package malms;
import x10.lang.Boolean;
/**
 * @author		Patrick Flick	<patrick.flick@gmail.com>
 * @version		0.1
 * @since		2012-05-24
 * 
 * Defines an element of the loser tree.
 */
public class Loser[T] {
	/**
	 * Whether the element is an artificial supremum.
	 */
	public var supremum : Boolean;
	/**
	 * The source sequence from which the key was taken.
	 */
	public var source : int;
	/**
	 * The key (value) of the element.
	 */
	public var key : T;
	
	/**
	 * Constructs a Loser.
	 * @param supremum	Sets the supremum.
	 * @param source	Sets the source.
	 * @param key		Sets the key.
	 */
	public def this(supremum:Boolean, source:int, key:T) {
		this.supremum = supremum;
		this.source = source;
		this.key = key;
	}
}
