package malms;
import x10.util.IndexedMemoryChunk;
import x10.array.Array;
/**
 * @author		Patrick Flick	<patrick.flick@gmail.com>
 * @version		0.1
 * @since		2012-05-27
 * 
 * Runs sequential tests for malms.
 */
public class Test {
    /**
     * Runs Tests for malms. 
	 * 
	 * @param args		Not needed.
     */
    public static def main(args: Array[String]) {
    	
		Console.OUT.println("======================");
		Console.OUT.println("    Testing Merge:");
		Console.OUT.println("======================");
		malms.TestMerge.test();
    	
		Console.OUT.println("");
		Console.OUT.println("======================");
		Console.OUT.println("    Testing Split:");
		Console.OUT.println("======================");
		malms.TestSplit.test();
        
		Console.OUT.println("");
		Console.OUT.println("======================");
		Console.OUT.println("  Testing seq malms:");
		Console.OUT.println("======================");
		malms.TestSequentialMalms.test();
		
		Console.OUT.println("");
		Console.OUT.println("======================");
		Console.OUT.println(" Testing async malms:");
		Console.OUT.println("======================");
		malms.TestAsyncMalms.test();
		
		Console.OUT.println("");
		Console.OUT.println("======================");
		Console.OUT.println(" Testing packet malms:");
		Console.OUT.println("======================");
		malms.TestPacketMalms.test();
		
		Console.OUT.println("");
		Console.OUT.println("======================");
		Console.OUT.println(" Timing packet malms:");
		Console.OUT.println("======================");
		malms.TimePacketMalms.time();
        
		Console.OUT.println("\n => Done <=");
    }
}
