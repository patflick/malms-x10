/**
 * The canonical "Hello, World" demo class expressed in X10
 */
public class Hello {

	static def changeNum(var x: Int)
	{
		x = 13;
	}
	
    /**
     * The main method for the Hello class
     */
    public static def main(Array[String]) {
        Console.OUT.println("Hello, World!");
        
        var x: Int = 7;
        changeNum(x);
        Console.OUT.println("x = " + x);
        
        //malms.Split.split(IndexedMemoryChunk.allocateUninitialized[int](5), IndexedMemoryChunk.allocateUninitialized[int](5), IndexedMemoryChunk.allocateUninitialized[int](5), 2, (x: Int,y: Int) => x.compareTo(y));
    }

}
